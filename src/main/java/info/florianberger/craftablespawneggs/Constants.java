

package info.florianberger.craftablespawneggs;

public final class Constants {

    // General constants
    static final int EggCountItem = 1;
    static final int EggCountExtended = 2;

    // Id for spawning eggs
    static final int CreeperSpawnEggId = 50;
    static final int SkeletonSpawnEggId = 51;
    static final int SpiderSpawnEggId = 52;
    static final int ZombieSpawnEggId = 54;
    static final int SlimeSpawnEggId = 55;
    static final int GhastSpawnEggId = 56;
    static final int ZombiePigmenSpawnEggId = 57;
    static final int EndermanSpawnEggId = 58;
    static final int CaveSpiderSpawnEggId = 59;
    static final int SilverfishSpawnEggId = 60;
    static final int BlazeSpawnEggId = 61;
    static final int MagmaCubeSpawnEggId = 62;
    public static final int BatSpawnEggId = 65;
    static final int WitchSpawnEggId = 66;
    static final int PigSpawnEggId = 90;
    static final int SheepSpawnEggId = 91;
    static final int CowSpawnEggId = 92;
    static final int ChickenSpawnEggId = 93;
    static final int SquidSpawnEggId = 94;
    static final int WolfSpawnEggId = 95;
    static final int MushroomCowSpawnEggId = 96;
    static final int OcelotSpawnEggId = 98;
    static final int HorseSpawnEggId = 100;
    public static final int VillagerSpawnEggId = 120;
    static final int SnowGolemSpawnEggId = 97;
    static final int IronGolemSpawnEggId = 99;

}
