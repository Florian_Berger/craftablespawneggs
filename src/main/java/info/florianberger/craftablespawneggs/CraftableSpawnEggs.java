package info.florianberger.craftablespawneggs;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

@Mod(modid = CraftableSpawnEggs.MODID, version = CraftableSpawnEggs.VERSION, canBeDeactivated = true, name = CraftableSpawnEggs.MODNAME)
public class CraftableSpawnEggs {
    static final String MODID = "CraftableSpawnEggs";
    static final String MODNAME = "Craftable Spawn Eggs";
    static final String VERSION = "1.0.0";

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        RegisterSpawnEggRecipes();
        RegisterSpawnerRecipes();
    }

    /**
     * Registers all crafting recipes for the spawn eggs.
     * All of them requires at least one egg and some other items
     */
    private void RegisterSpawnEggRecipes() {

        //region Enemy mobs

        //region Creeper

        // Creeper skull
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountExtended, Constants.CreeperSpawnEggId),
                Items.egg, new ItemStack(Items.skull, 1, 4));

        // Gunpowder
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.CreeperSpawnEggId),
                Items.egg, Items.gunpowder);

        //endregion

        //region Skeleton

        // Bone
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.SkeletonSpawnEggId),
                Items.egg, Items.bone);

        // Skeleton skull
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountExtended, Constants.SkeletonSpawnEggId),
                Items.egg, new ItemStack(Items.skull, 1));

        //endregion Skeleton

        //region Spider

        // Spider eye
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.SpiderSpawnEggId),
                Items.egg, Items.spider_eye);

        //endregion Spider

        //region Zombie

        // Rotten flesh
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.ZombieSpawnEggId),
                Items.egg, Items.rotten_flesh);

        // Zombie skull
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountExtended, Constants.ZombieSpawnEggId),
                Items.egg, new ItemStack(Items.skull, 1, 2));

        //endregion Zombie

        //region Slime

        // Slime balls (2 for egg)
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.SlimeSpawnEggId),
                Items.egg, new ItemStack(Items.slime_ball, 2));

        //endregion Slime

        //region Ghast

        // Ghast tear
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.GhastSpawnEggId),
                Items.egg, Items.ghast_tear);

        //endregion Ghast

        //region Zombie pigmen

        // Rotten flesh and raw porkchop
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.ZombiePigmenSpawnEggId),
                Items.egg, Items.rotten_flesh, Items.porkchop);

        //endregion Zombie pigmen

        //region Enderman

        // Enderperle - Extended number of eggs
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountExtended, Constants.EndermanSpawnEggId),
                Items.egg, Items.ender_pearl);

        //endregion Enderman

        //region Cave spider

        // Spider eye and stone
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.CaveSpiderSpawnEggId),
                Items.egg, Items.spider_eye, Blocks.stone);

        //endregion Cave spider

        //region Silverfish

        // Silverfish currently not craftable - recipe not defined
        //GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountExtended, Constants.SilverfishSpawnEggId),
        //        Items.egg, Items.???);

        //endregion Silverfish

        //region Blaze

        // Blaze rod
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountExtended, Constants.BlazeSpawnEggId),
                Items.egg, Items.blaze_rod);

        //endregion Blaze

        //region Magmacube

        // Magma creme
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.MagmaCubeSpawnEggId),
                Items.egg, Items.magma_cream);

        //endregion Magmacube

        //region Bat

        // Bat currently not craftable - recipe not defined
        //GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountExtended, Constants.BlazeSpawnEggId),
        //        Items.egg, Items.???);

        //endregion Bat

        //region Witch

        // Glass bottle
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.WitchSpawnEggId),
                Items.egg, Items.glass_bottle);

        //endregion Witch

        //endregion Enemy mobs

        //region Friendly mobs

        //region Pig

        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.PigSpawnEggId),
                Items.egg, Items.porkchop);

        //endregion Pig

        //region Sheep

        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.SheepSpawnEggId),
                Items.egg, Blocks.wool);

        //endregion Sheep

        //region Cow

        // Leather
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.CowSpawnEggId),
                Items.egg, Items.leather);

        //endregion Cow

        //region Chicken

        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.ChickenSpawnEggId),
                Items.egg, Items.feather);

        //endregion Chicken

        //region Squid

        // Ink sak
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.SquidSpawnEggId),
                Items.egg, new ItemStack(Items.dye, 1, 0));

        //endregion Squid

        //region Wolf

        // Bone and rotten flesh
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.WolfSpawnEggId),
                Items.egg, Items.bone, Items.rotten_flesh);

        //endregion Wolf

        //region Mushroom cow

        // Red mushroom
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.MushroomCowSpawnEggId),
                Items.egg, Blocks.red_mushroom);

        // Brown mushroom
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.MushroomCowSpawnEggId),
                Items.egg, Blocks.brown_mushroom);

        //endregion Mushroom cow

        //region Ocelot

        // Fish
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.OcelotSpawnEggId),
                Items.egg, Items.fish);

        //endregion Ocelot

        //region Horse

        // Leather and saddle
        GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountExtended, Constants.HorseSpawnEggId),
                Items.egg, Items.leather, Items.saddle);

        //endregion Horse

        //region Villager

        // Villager currently not spawnable - recipe not defined
        //GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, Constants.EggCountItem, Constants.VillagerSpawnEggId),
        //        Items.egg, Items.porkchop);

        //endregion Villager

        //region Snow golem

        // 2 snow blocks and 1 pumpkin
        GameRegistry.addRecipe(new ItemStack(Items.spawn_egg, 1, 97),
                "AB ", " C ", " C ", 'A', Items.egg, 'B', Blocks.pumpkin, 'C', Blocks.snow);

        //endregion Snow golem

        //region Iron golem

        // 4 iron blocks and 1 pumpkin
        GameRegistry.addRecipe(new ItemStack(Items.spawn_egg, 1, 99),
                "AB ", "CCC", " C ", 'A', Items.egg, 'B', Blocks.pumpkin, 'C', Blocks.iron_block);

        //endregion Iron golem

        //endregion Friendly mobs
    }

    /**
     * Registers all crafting recipes for the mob spawners
     * All of them requires 1 block emerald, 4 blocks iron and 4 spawning eggs of the entity the spawner should be
     */
    private void RegisterSpawnerRecipes() {

        //region Skeleton

        GameRegistry.addRecipe(new ItemStack(Blocks.mob_spawner, 1, 51),
                "ACA", "BBB", "ABA", 'A', Blocks.iron_block, 'B', new ItemStack(Items.spawn_egg, 1, 51), 'C', Blocks.emerald_block);

        //endregion Skeleton

        //region Zombie

        GameRegistry.addRecipe(new ItemStack(Blocks.mob_spawner, 1, 54),
                "ACA", "BBB", "ABA", 'A', Blocks.iron_block, 'B', new ItemStack(Items.spawn_egg, 1, 54), 'C', Blocks.emerald_block);

        //endregion Zombie

        //region Spider

        GameRegistry.addRecipe(new ItemStack(Blocks.mob_spawner, 1, 52),
                "ACA", "BBB", "ABA", 'A', Blocks.iron_block, 'B', new ItemStack(Items.spawn_egg, 1, 52), 'C', Blocks.emerald_block);

        //endregion Spider

        //region Cave spider

        GameRegistry.addRecipe(new ItemStack(Blocks.mob_spawner, 1, 59),
                "ACA", "BBB", "ABA", 'A', Blocks.iron_block, 'B', new ItemStack(Items.spawn_egg, 1, 59), 'C', Blocks.emerald_block);

        //endregion Cave spider

        //region Enderman

        GameRegistry.addRecipe(new ItemStack(Blocks.mob_spawner, 1, 58),
                "ACA", "BBB", "ABA", 'A', Blocks.iron_block, 'B', new ItemStack(Items.spawn_egg, 1, 58), 'C', Blocks.emerald_block);

        //endregion Enderman 
    }
}
