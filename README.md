This modification is adding the possibility to craft spawning eggs (nearly all entities) and spawners (zombies, skeletons, spiders, cave spiders) in Minecraft. It is perfect to make a mob trap or something else.

## Issue reporting
For issue reportings I need the following informations:

* Used *"Minecraft"* version

* Used *"CraftableSpawnEggs"* version

* Version of *"Forge"*

* List of other used modifications

## Licenses
The project is licensed under the [MIT License](LICENSE.md)!

You are allowed to use the mod in your modpack without asking me.
